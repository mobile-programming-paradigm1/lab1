import 'dart:io';

void main() {
  String para =
      '''Dart Constants are the objects or variables whose values can’t change or modify during the execution of the program. 
Their use case is when we want a particular value to remain unchanged throughout the program’s execution.''';
//get string length
  int len = para.length;
  print("How many words are there : ");
  print(len);
  print("How many times does the word appear :");
//split word with space
  var list = para.split(" ");
//count by Loopl
  Map map = {};
  list.forEach((ele) {
    if (map.containsKey(ele)) {
      map[ele] = map[ele] + 1;
    } else {
      map[ele] = 1;
    }
  });
  map.forEach((key, value) {
    print(
        key.toString() + " -> Found --> " + value.toString() + " times" + '\n');
  });
}
