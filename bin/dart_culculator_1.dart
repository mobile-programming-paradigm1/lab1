import 'dart:io';

void main() {
  print(
      " \n MENU \n Select the choice you want to perform : \n 1. ADD \n 2. SUBTRACT \n 3. MULTIPLY \n 4. DIVIDE \n 5. EXIT \n Choice you want to enter : ");
  var num = int.parse(stdin.readLineSync()!);

  print(" \n Enter the value for x : ");
  var x = int.parse(stdin.readLineSync()!);

  print(" \n Enter the value for y : ");
  var y = int.parse(stdin.readLineSync()!);

  switch (num) {
    case 1:
      var Add = x + y;
      print(' \n Sum of the two numbers is : ');
      print(Add);
      break;

    case 2:
      var SUBTRACT = x - y;
      print(' \n Minus of the two numbers is : : ');
      print(SUBTRACT);
      break;

    case 3:
      var MULTIPLY = x * y;
      print(' \n Product of the two numbers is : ');
      print(MULTIPLY);
      break;

    case 4:
      var DIVIDE = x % y;
      print(' \n Quotient of the two numbers is : ');
      print(DIVIDE);
      break;

    case 5:
      print("EXIT ");
      break;
  }
}
